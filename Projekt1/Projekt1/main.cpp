#include <iostream>
#include <cstdlib>
#include <ctime>
#include <fstream>
#include <string>
#include <cstdio>

using namespace std;

int **torz_tablice(int n, int m, int k)
{
	int **tab = new int *[n];        //alokacja pamieci tablicy dwuwymiarowej (wierszy)
	for (int i = 0; i < n; ++i)
	{
		tab[i] = new int[m];          //alokacja pamieci (kolumn)
		for (int j = 0; j < m; ++j)   //wpisanie wartosci losowej do tablicy z zakresu 0-k
			tab[i][j] = rand() % (k + 1);
	}
	return tab;
}

bool usun_tablice(int **tab,int n)
{
	if (tab!=NULL)
	{
		for (int i(0); i < n; ++i)     //usuwanie tablicy
			delete[] tab[i];
		delete[] tab;
		tab = NULL;
		return true;
	}
	return false;
}

bool wypisz_tablice(int **tab, int n, int m)
{
	if (tab != NULL)
	{
		for (int i = 0; i < n; ++i)       //wypisanie tablicy
		{
			for (int j = 0; j < m; ++j)
				cout << tab[i][j] << '\t';
			cout << endl;
		}
		return true;
	}
	return false;
}

int znajdz_maksimum(int **tab, int n, int m)
{
	int maksimum=0;                        //wartosc aktulnbie najwieksza
	for (int i = 0; i < n; ++i)       
		for (int j = 0; j < m; ++j)
			if (tab[i][j] > maksimum)
				maksimum = tab[i][j];	
	return maksimum;
}

bool zapisz_tablice(int **tab, int n, int m, string nazwa_pliku_wyj)
{
	fstream plik;
	plik.open(nazwa_pliku_wyj, ios::out); //otwieranie pliku

	if (tab != NULL && plik.good())
	{
		for (int i = 0; i < n; ++i)       
		{
			for (int j = 0; j < m; ++j)
				plik << tab[i][j];     //zapisywanie pliku
			plik << endl;
		}
		plik.close();
		return true;
	}
	plik.close();
	return false;
}

int **wczytaj_tablice( int *n, int *m, string nazwa_pliku_wej)
{
	string linia;
	string tekst;
	fstream plik;
	int k = 0;
	plik.open(nazwa_pliku_wej, ios::in); //otwieranie pliku
	if (plik.good()) 
	{                               //spr czy otwarto
		getline(plik,linia);			// pobieranie 1 lini w celu sprawdzenia luczby kolumn oraz rozpoczecie czytania calego pliku
		tekst = linia;
		(*m) = linia.size();            //spr  liczby kolumn
		while (!plik.eof())   
		{
			getline(plik, linia);        // czytanie calego pliku linia po lini i dodawanie do strina teklkst
			tekst = tekst + linia;
		}
		(*n) = tekst.size();        //ilosc wszystkich znkakow
		if ((*m) != 0)
			(*n) = (*n) / (*m);    //obliczanie liczby wierszy
		else
			cout << "rozmiar lini jesty 0" << endl;
		int **tab = new int *[*n];            //alokacja pamieci tablicy dwuwymiarowej (wierszy)
		for (int i = 0; i < (*n); ++i)
		{
			tab[i] = new int[*m];             //alokacja pamieci (kolumn)
			for (int j = 0; j < (*m); ++j)    
			{
				tab[i][j] = tekst[k] - '0';     //przepisanie warosci tekst do tablicy intow i odjecie 0 zeby se zgadzalo asci z wartoscia inta
				++k;						
			}
		}
	plik.close();          //zamykanie pliku
	return tab;
	}
	plik.close();				//zamykanie pliku
	return NULL;
}

bool zapisz_tablice_bin(int **tab, int n, int m, string nazwa_pliku_wyj)
{

	ofstream plik;
	plik.open(nazwa_pliku_wyj, ios::binary); //otwieranie pliku

	if (tab != NULL && plik.good())
	{
		for (int i = 0; i < n; ++i)
		{
			for (int j = 0; j < m; ++j)
				plik.put(tab[i][j]);          //zapisywanie pliku
			plik.put('/n');
		}
		plik.close();
		return true;
	}
		cout << "plik nie good()" << endl;
		plik.close();
		return false;
}

int **wczytaj_tablice_bin(int *n, int *m, string nazwa_pliku_wej)
{

	string linia;
	string tekst;
	ifstream plik;
	int k = 0;
	plik.open(nazwa_pliku_wej, ios::binary); //otwieranie pliku
	if (plik.good()) {

		int **tab = new int *[*n];            //alokacja pamieci tablicy dwuwymiarowej (wierszy)
		for (int i = 0; i < (*n); ++i)
		{
			tab[i] = new int[*m];             //alokacja pamieci (kolumn)
			for (int j = 0; j < (*m); ++j)    //wpisanie wartosci losowej do tablicy z zakresu 0-k
			{
				char* temp = new char[sizeof(int)]; // tymczasowy bufor na dane
				plik.read(temp, sizeof(int)); // wczytujemy dane do bufora
				tab[i][j] = (int)(temp); // rzutujemy zawarto�� bufora na typ int
			}
		}
		plik.close();
		return tab;
	}
	cout << "plik nie good()" << endl;
	plik.close();
	return NULL;
}

int potega(int x, int p)
{
	if(p == 0)
		return 1;
	else
		return x = x*potega(x, --p);
}

int silnia(int x)
{
	if (x<2)
		return 1;
	else
		return x*silnia(x-1);
}

bool jestPal(string test)
{
	if (test.size() < 3)
		return true;
	else
	{
		if (test[0] == test[test.size()-1])
		{
			test.erase(test.size()-1, 1);
			test.erase(0, 1);
			jestPal(test);
		}
		else
			return false;
	}
}

int main()
{
	int n = 0, m = 0, k = 0,x=0,p=0;
	srand(time(NULL));
	int **tab = NULL;
	string nazwa_pliku_wej;
	string nazwa_pliku_wyj;
	string pal, cos;

	int opcja;
	do {
		system("cls");
		cout << "1.Podaj liczbe wierszy" << endl;
		cout << "2.Podaj liczbe kolumn" << endl;
		cout << "3.podaj zakres losowo�ci" << endl;
		cout << "4.stworz tablice" << endl;
		cout << "5.wyswietl tablice" << endl;
		cout << "6.znajdz najwieksza liczbe" << endl;
		cout << "7.zapisz tablice do pliku tekstowego" << endl;
		cout << "8.wczytaj tablice z pliku tekstowego" << endl;
		cout << "9.zapisz tablice do pliku binarnego" << endl;
		cout << "10.wczytaj tablice z pliku binarnego" << endl;
		cout << "11.potega" << endl;
		cout << "12.silnia" << endl;
		cout << "13.Palindrom" << endl;
		cout << "0.Koniec" << endl;
		cout << "Wybierz opcje: ";
			cin >> opcja;
			switch (opcja)
			{
			case 0: break;
			case 1:
				cout << "Liczba wierszy: ";
				cin >> n;
				break;
			case 2:
				cout << "Liczba kolumn: ";
				cin >> m;
				break;
			case 3:
				cout << "Zakres losowosci: ";
				cin >> k;
				break;
			case 4:
				tab = torz_tablice(n, m, k);
				cout << "macierz tablice " << endl;
				system("PAUSE");
				break;
			case 5:
				wypisz_tablice(tab, n, m);
				system("PAUSE");
				break;
			case 6:
				cout << "najwieksza liczba to: " << znajdz_maksimum(tab, n, m) << endl;
				system("PAUSE");
				break;
			case 7:
				cout << "podaj nazwe pliku pod ktorym zapisac tablice: ";
				cin >> nazwa_pliku_wyj;
				if (zapisz_tablice(tab, n, m, nazwa_pliku_wyj))
					cout << "zapisano poprawnie pod nazwa: " << nazwa_pliku_wyj << endl;
				system("PAUSE");
				break;
			case 8:
				cout << "podaj nazwe pliku z ktorego wczytac tablice: ";
				cin >> nazwa_pliku_wej;
				tab = wczytaj_tablice(&n, &m, nazwa_pliku_wej);
				if (tab == NULL)
					cout << "nie powiodlo sie" << endl;
				else
					cout << "wczytano tablice z " << n << " wierszami i " << m << " kolumnami" << endl;
				system("PAUSE");
				break;
			case 9:
				cout << "podaj nazwe pliku pod ktorym zapisac tablice: ";
				cin >> nazwa_pliku_wyj;
				if (zapisz_tablice_bin(tab, n, m, nazwa_pliku_wyj))
					cout << "zapisano poprawnie pod nazwa: " << nazwa_pliku_wyj << endl;
				system("PAUSE");
			case 10:
				cout << "podaj nazwe pliku z ktorego wczytac tablice: ";
				cin >> nazwa_pliku_wej;
				tab = wczytaj_tablice_bin(&n, &m, nazwa_pliku_wej);
				if (tab == NULL)
					cout << "nie powiodlo sie" << endl;
				else
					cout << "wczytano tablice z " << n << " wierszami i " << m << " kolumnami" << endl;
				system("PAUSE");
				break;
			case 11:
				cout << "liczba: ";
				cin >> x;
				cout << "potega: ";
				cin >> p;
				cout << "wynik to: " << potega(x, p) << endl;
				system("PAUSE");
				break;
			case 12:
				cout << "Liczba silni: ";
				cin >> x;
				cout << "wynik to: " << silnia(x) << endl;
				system("PAUSE");
				break;
			case 13:
				cout << "podaj slowo: ";
				cin >> pal;
				if(jestPal(pal))
					cout << "slowo jest Palindromem" << endl;
				else
					cout << "slowo nie jest Palindromem" << endl;
				system("PAUSE");
				break;
			default:
				cout << "zly parametr, nie znaleziono polecenia" << endl;
				system("PAUSE");
			break;

			}

		
	} while (opcja != 0);

	usun_tablice(tab, n);

	
//	system("PAUSE");
	return 0;
}